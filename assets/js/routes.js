import Vue from 'vue';
import VueRouter from 'vue-router';

import Game from './pages/game';
import Player from './pages/player';
import Team from './pages/team';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/game/',
            name: 'Game',
            component: Game
        },
        {
            path: '/player/',
            name: 'Player',
            component: Player
        },
        {
            path: '/team/',
            name: 'Team',
            component: Team
        },
    ]
});

export default router;