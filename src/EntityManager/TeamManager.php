<?php

namespace App\EntityManager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class TeamManager
{
    /** @var EntityManagerInterface $em */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
}